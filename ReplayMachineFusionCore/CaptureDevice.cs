﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Runtime.InteropServices.ComTypes;
using System.Text;
using System.Threading.Tasks;
using DirectShowLib;
using Microsoft.VisualStudio;

namespace ReplayMachineFusion.Core
{
	/// <summary>
	/// Устройство захвата.
	/// 
	/// Представляет из себя устройство захвата, которое является источником видео-сигнала.
	/// </summary>
	public class CaptureDevice : IDisposable
	{
		private const int ONE_SECOND = 10000000;

		private IPin capturePin;
		private DsDevice directShowDevice;
		private IBaseFilter sourceFilter;
		private List<VideoFormat> supportedVideoFormats;

		private CaptureDevice(DsDevice directShowDevice)
		{
			this.directShowDevice = directShowDevice;

			Guid iid = typeof(IBaseFilter).GUID;
			object filterObject;
			directShowDevice.Mon.BindToObject(null, null, ref iid, out filterObject);
			sourceFilter = filterObject as IBaseFilter;
			capturePin = DsFindPin.ByCategory(sourceFilter, PinCategory.Capture, 0);

			// Определяем поддерживаемые форматы, удаляя дубликаты.
			supportedVideoFormats = new List<VideoFormat>(GetSupportedVideoFormats());
			supportedVideoFormats = SupportedVideoFormats.Distinct().ToList();
		}

		/// <summary>
		/// Поддерживаемые этим устройством форматы захвата.
		/// </summary>
		public IReadOnlyList<VideoFormat> SupportedVideoFormats
		{
			get
			{
				return supportedVideoFormats;
			}
		}

		/// <summary>
		/// Символьная ссылка.
		/// 
		/// Уникальный идентификатор устройства в системе.
		/// </summary>
		public string SymbolicLink
		{
			get
			{
				return directShowDevice.DevicePath;
			}
		}
		
		/// <summary>
		/// Имя устройства в системе.
		/// </summary>
		public string Title
		{
			get
			{
				return directShowDevice.Name;
			}
		}

		public void Dispose()
		{
			Marshal.ReleaseComObject(capturePin);
			Marshal.ReleaseComObject(sourceFilter);
			directShowDevice.Dispose();
		}
		
		/// <summary>
		/// Запрашивает у системы доступные устройства захвата.
		/// </summary>
		/// <returns>Список доступных устройств.</returns>
		public static IReadOnlyList<CaptureDevice> GetAvailableCaptureDevices()
		{
			var devices = new List<CaptureDevice>();

			foreach (var dev in DsDevice.GetDevicesOfCat(FilterCategory.VideoInputDevice)) {
				devices.Add(new CaptureDevice(dev));
			}

			return devices;
		}

		/// <summary>
		/// Определяет поддерживаемые устройством форматы видео.
		/// </summary>
		/// <returns>Поддерживаемые форматы.</returns>
		private IEnumerable<VideoFormat> GetSupportedVideoFormats()
		{
			IEnumMediaTypes enumMediaTypes;
			int hr = capturePin.EnumMediaTypes(out enumMediaTypes);
			DsError.ThrowExceptionForHR(hr);

			AMMediaType[] rawMediaType = new AMMediaType[1];
			while (enumMediaTypes.Next(1, rawMediaType, IntPtr.Zero) == VSConstants.S_OK) {
				// Обходим поддерживаемые выходным pin'ом медиа типы.

				var mediaType = rawMediaType[0];

				int width = 0, height = 0;
				double fps = 0;
				bool interlaced = false;
				if (mediaType.formatType == FormatType.VideoInfo) {
					// VIDEOINFOHEADER

					var formatBlock = new VideoInfoHeader();
					Marshal.PtrToStructure(mediaType.formatPtr, formatBlock);

					width = formatBlock.BmiHeader.Width;
					height = formatBlock.BmiHeader.Height;
					fps = Math.Round((double)ONE_SECOND / formatBlock.AvgTimePerFrame, 2, MidpointRounding.AwayFromZero);
				} else if (mediaType.formatType == FormatType.VideoInfo2) {
					// VIDEOINFOHEADER2

					var formatBlock = new VideoInfoHeader2();
					Marshal.PtrToStructure(mediaType.formatPtr, formatBlock);

					width = formatBlock.BmiHeader.Width;
					height = formatBlock.BmiHeader.Height;
					fps = Math.Round((double)ONE_SECOND / formatBlock.AvgTimePerFrame, 2, MidpointRounding.AwayFromZero);
					interlaced = formatBlock.InterlaceFlags.HasFlag(AMInterlace.IsInterlaced);
				}

				var videoFormat = new VideoFormat();

				videoFormat.Resolution = new VideoFormat.ResolutionType {
					Width = width,
					Height = height
				};
				videoFormat.FrameRate = fps;
				videoFormat.Scanning = (interlaced) ? VideoFormat.ScanningType.Interlaced :
					VideoFormat.ScanningType.Progressive;

				if (mediaType.subType == MediaSubType.DVSD) {
					// DV

					videoFormat.VideoCodec = VideoFormat.VideoCodecType.DV;
					videoFormat.Scanning = VideoFormat.ScanningType.Interlaced;

					if (fps == 25) {
						videoFormat.TvStandart = VideoFormat.TvStandartType.PAL;
					} else if (fps == 29.97) {
						videoFormat.TvStandart = VideoFormat.TvStandartType.NTSC;
					}
				} else if (mediaType.subType == MediaSubType.Mpeg2Video) {
					videoFormat.VideoCodec = VideoFormat.VideoCodecType.MPEG2;
				} else if (mediaType.subType == MediaSubType.MJPG) {
					videoFormat.VideoCodec = VideoFormat.VideoCodecType.MJPEG;
				}

				DsUtils.FreeAMMediaType(mediaType);
				
				yield return videoFormat;
			}

			Marshal.ReleaseComObject(enumMediaTypes);
		}
	}
}
