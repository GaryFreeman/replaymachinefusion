using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace ReplayMachineFusion.Core.MediaStorage
{
	[Serializable]
	public class MediaStorageCreationFailedException : MediaStorageException
	{
		public MediaStorageCreationFailedException()
		{
		}

		public MediaStorageCreationFailedException(string message)
			: base(message)
		{
		}

		public MediaStorageCreationFailedException(string message, Exception inner)
			: base(message, inner)
		{
		}

		protected MediaStorageCreationFailedException(SerializationInfo info, StreamingContext context)
			: base(info, context)
		{
		}
	}
}
