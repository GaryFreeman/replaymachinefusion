﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using System.Reflection;
using ReplayMachineFusion.Core.MediaStorage.Xml;
using NLog;

namespace ReplayMachineFusion.Core.MediaStorage
{
	/// <summary>
	/// Хранилище медиа-данных.
	///
	/// Хранилище медиа-данных представляет собой специальную директорию, в которой хранится видео и аудио в виде
	/// циклического буфера.
	/// </summary>
	public class MediaStorage : IDisposable
	{
		/// <summary>
		/// Тип содержимого хранилища, т.е. что в нем хранится.
		/// </summary>
		public enum ContentType
		{
			/// <summary>
			/// Ведется запись только видео.
			/// </summary>
			Video,

			/// <summary>
			/// Записывается видео и аудио.
			/// </summary>
			VideoAndAudio
		}

		private static Logger logger = LogManager.GetCurrentClassLogger();

		/// <summary>
		/// Создает хранилище на диске. Если оно уже существует, возвращает существующее хранилище.
		/// </summary>
		/// <param name="path">Путь к хранилищу.</param>
		/// <param name="content"><see cref="Content"/></param>
		/// <returns>Хранилище.</returns>
		/// <exception cref="MediaStorageBusyException">
		///	Хранилище уже загружено (если открывается существующее хранилище).
		/// </exception>
		/// <exception cref="MediaStorageNotConsistentException">
		///	Нарушена целостность хранилища (если открывается существующее хранилище).
		///	</exception>
		///	<exception cref="MediaStorageCreationFailedException">
		///	Ошибка при создании хранилища (недостаточно прав).
		///	</exception>
		public static MediaStorage CreateMediaStorage(DirectoryInfo path, ContentType content = ContentType.Video)
		{
			logger.Debug("Запрошено создание хранилища ({0}, {1}).", path, content);

			if (CheckExistance(path)) {
				// Загружаем существующее хранилище.

				return new MediaStorage(path);
			} else {
				// Создаем хранилище "с нуля".

				return new MediaStorage(path, content);
			}
		}

		/// <summary>
		/// Открывает существующее хранилище.
		/// </summary>
		/// <param name="path">Путь к хранилищу.</param>
		/// <returns>Хранилище.</returns>
		/// <exception cref="MediaStorageNotFoundException">Хранилище не найдено.</exception>
		/// <exception cref="MediaStorageBusyException">
		///	Хранилище уже загружено.
		/// </exception>
		/// <exception cref="MediaStorageNotConsistentException">
		///	Нарушена целостность хранилища.
		///	</exception>
		public static MediaStorage OpenMediaStorage(DirectoryInfo path)
		{
			logger.Debug("Запрошено открытие хранилища ({0}).", path);

			if (!CheckExistance(path)) {
				throw new MediaStorageNotFoundException("Хранилище не найдено.");
			}

			return new MediaStorage(path);
		}

		/// <summary>
		/// Проверяет, существует ли хранилище по указанному пути.
		/// </summary>
		/// <param name="path">Путь к хранилищу.</param>
		/// <returns>true, если хранилище существует.</returns>
		private static bool CheckExistance(DirectoryInfo path)
		{
			return path.Exists && path.GetFiles(Manifest.fileName).Length != 0;
		}

		private bool disposed = false;
		private FileStream manifest;
		private VideoFormat videoFormat;
		private TimeSpan duration;
		private ContentType content;

		/// <summary>
		/// Конструктор хранилища "с нуля", т.е. необходимо создать его на диске.
		/// </summary>
		/// <param name="path">Путь к хранилищу.</param>
		/// <param name="content"><see cref="Content"/></param>
		/// <exception cref="MediaStorageCreationFailedException">
		///	Ошибка при создании хранилища (недостаточно прав).
		///	</exception>
		private MediaStorage(DirectoryInfo path, ContentType content)
		{
			logger.Info("Создание хранилища \"с нуля\" ({0}, {1}).", path, content);

			Path = path;
			Content = content;

			try {
				CreateStorageOnDisk();
			} catch (UnauthorizedAccessException ex) {
				throw new MediaStorageCreationFailedException(String.Format("Невозможно создать хранилище." +
					" Вы имеете недостаточно прав для совершения данного действия ({0}).", Path), ex);
			} catch (DirectoryNotFoundException ex) {
				throw new MediaStorageCreationFailedException(String.Format("Невозможно создать хранилище." +
					" Неверно указанный путь ({0}).", Path), ex);
			} catch (PathTooLongException ex) {
				throw new MediaStorageCreationFailedException(String.Format("Невозможно создать хранилище." +
					" Слишком длинный путь или имя файла ({0}).", Path), ex);
			} catch (Exception ex) {
				throw new MediaStorageCreationFailedException(String.Format("Невозможно создать хранилище. ({0}).", Path), ex);
			}

			InitializeStorage();
		}

		/// <summary>
		/// Конструктор хранилища, которое уже существует на диске.
		/// </summary>
		/// <param name="path">Путь к хранилищу.</param>
		/// <exception cref="MediaStorageBusyException">Хранилище уже загружено.</exception>
		/// <exception cref="MediaStorageNotConsistentException">Нарушена целостность хранилища.</exception>
		private MediaStorage(DirectoryInfo path)
		{
			logger.Info("Загрузка хранилища с диска ({0}).", path);

			Path = path;

			InitializeStorage();
		}

		/// <summary>
		/// Путь к хранилищу.
		/// </summary>
		public DirectoryInfo Path
		{
			get;
			private set;
		}

		/// <see cref="ContentType"/>
		public ContentType Content
		{
			get
			{
				return content;
			}

			set
			{
				content = value;
			}
		}

		/// <summary>
		/// Объем хранилища, т.е. длина его циклического буфера.
		/// </summary>
		public TimeSpan Duration
		{
			get
			{
				return duration;
			}

			set
			{
				if (duration.Seconds != 0) {
					throw new ArgumentException("Объем хранилища не должен содержать неполных минут.");
				}

				duration = value;
			}
		}

		/// <summary>
		/// Формат видео, хранимого в хранилище.
		/// </summary>
		public VideoFormat VideoFormat
		{
			get
			{
				return videoFormat;
			}

			set
			{
				videoFormat = value;
			}
		}

		/// <summary>
		/// Создает базовую структуру нового хранилища.
		/// </summary>
		private void CreateStorageOnDisk()
		{
			logger.Info("Создаем хранилище на диске ({0}).", Path);

			Path.Create();

			using (var writer = File.CreateText(System.IO.Path.Combine(Path.FullName, Manifest.fileName))) {
				writer.Write(GetManifestTemplate());
			}
		}

		/// <summary>
		/// Инициализация хранилища.
		/// </summary>
		/// <exception cref="MediaStorageBusyException">Хранилище уже загружено.</exception>
		/// <exception cref="MediaStorageNotConsistentException">Нарушена целостность хранилища.</exception>
		private void InitializeStorage()
		{
			var manifestPath = System.IO.Path.Combine(Path.FullName, Manifest.fileName);

			try {
				manifest = new FileStream(manifestPath, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.Read);
			} catch (IOException ex) {
				// Хранилище уже загружено.

				throw new MediaStorageBusyException("Хранилище уже загружено.", ex);
			}

			logger.Debug("Загрузка данных из манифеста ({0}).", manifestPath);
			LoadManifest();
			CheckIntegrity();
		}

		/// <summary>
		/// Загружает данные из манифеста.
		/// </summary>
		/// <exception cref="MediaStorageNotConsistentException">
		/// Выбрасывается при ошибке валидации манифеста.
		/// </exception>
		/// <exception cref="ObjectDisposedException">
		/// Выбрасывается при ошибке валидации манифеста.
		/// </exception>
		public void LoadManifest()
		{
			CheckDisposed();

			// Настраиваем валидацию.
			var settings = new XmlReaderSettings();
			settings.ValidationType = ValidationType.Schema;
			settings.Schemas.Add(GetManifestSchema());

			manifest.Seek(0, SeekOrigin.Begin);
			var reader = XmlReader.Create(manifest, settings);

			try {
				// Загружаем манифест.
				var serializer = new XmlSerializer(typeof(Xml.Manifest), Manifest.xmlNamespace);
				var manifestMapping = (Xml.Manifest)serializer.Deserialize(reader);
				manifestMapping.LoadIntoMediaStorage(this);
			} catch (InvalidOperationException ex) {
				throw new MediaStorageNotConsistentException("Хранилище повреждено: ошибка валидации манифеста.", ex);
			} finally {
				reader.Dispose();
			}
		}

		/// <summary>
		/// Сохраняет данные в манифест.
		/// </summary>
		/// <exception cref="ObjectDisposedException">Выбрасывается, если был ранее вызван метод Dispose.</exception>
		public void SaveManifest()
		{
			CheckDisposed();

			var manifestObject = new Manifest();
			manifestObject.LoadFromMediaStorage(this);

			manifest.SetLength(0);
			using (var writer = XmlWriter.Create(manifest, new XmlWriterSettings() { Indent = true, IndentChars = "	" })) {
				var serializer = new XmlSerializer(typeof(Xml.Manifest), Manifest.xmlNamespace);
				serializer.Serialize(writer, manifestObject);
			}
		}

		/// <summary>
		/// Проверяет целостность хранилища. Предполагается, что хранилище существует.
		/// </summary>
		/// <exception cref="MediaStorageNotConsistentException">
		/// Выбрасывается при нарушенной целостности хранилища.
		/// </exception>
		private void CheckIntegrity()
		{
			logger.Info("Проверка целостности - целостность не нарушена.");

			// TODO: Добавить проверку на корректность.
			// throw new MediaStorageNotConsistentException("Хранилище повреждено.");
		}

		/// <summary>
		/// Возвращает схему манифеста.
		/// </summary>
		/// <returns>Схема.</returns>
		private XmlSchema GetManifestSchema()
		{
			var assembly = Assembly.GetExecutingAssembly();
			var schemaResourceName = "ReplayMachineFusion.Core.MediaStorage.Xml.MediaStorage.xsd";
			var schema = XmlSchema.Read(assembly.GetManifestResourceStream(schemaResourceName), (s, e) => {
			});

			return schema;
		}

		/// <summary>
		/// Возвращает шаблон манифеста.
		/// </summary>
		/// <returns>Шаблон манифест в текстовом виде.</returns>
		private string GetManifestTemplate()
		{
			var assembly = Assembly.GetExecutingAssembly();
			var resourceName = "ReplayMachineFusion.Core.MediaStorage.Xml.MediaStorageTemplate.xml";

			using (Stream stream = assembly.GetManifestResourceStream(resourceName))
			using (StreamReader reader = new StreamReader(stream)) {
				return reader.ReadToEnd();
			}
		}

		/// <summary>
		/// Проверяет состояние объекта, если он уже освобожден ("Disposed"), то выбрасывает исключение.
		/// </summary>
		/// <exception cref="ObjectDisposedException">Выбрасывается, если был ранее вызван метод Dispose.</exception>
		private void CheckDisposed()
		{
			if (disposed) {
				throw new ObjectDisposedException(GetType().FullName);
			}
		}

		public void Dispose()
		{
			disposed = true;

			logger.Info("Выгрузка хранилища ({0}).", Path);

			manifest.Close();
		}
	}
}
