using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace ReplayMachineFusion.Core.MediaStorage
{
	[Serializable]
	public class MediaStorageNotConsistentException : MediaStorageException
	{
		public MediaStorageNotConsistentException()
		{
		}

		public MediaStorageNotConsistentException(string message)
			: base(message)
		{
		}

		public MediaStorageNotConsistentException(string message, Exception inner)
			: base(message, inner)
		{
		}

		protected MediaStorageNotConsistentException(SerializationInfo info, StreamingContext context)
			: base(info, context)
		{
		}
	}
}
