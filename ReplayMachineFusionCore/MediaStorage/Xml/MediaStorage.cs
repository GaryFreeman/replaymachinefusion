﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace ReplayMachineFusion.Core.MediaStorage.Xml
{
	[XmlRoot("MediaStorage")]
	public class Manifest
	{
		public static readonly string fileName = "MediaStorage.xml";
		public static readonly string xmlNamespace = "http://tempuri.org/MediaStorage.xsd";

		[XmlElement]
		public MediaStorage.ContentType Content
		{
			get;
			set;
		}

		[XmlIgnore]
		public TimeSpan Duration
		{
			get;
			set;
		}

		[XmlElement("Duration")]
		public string DurationString
		{
			get
			{
				return String.Format("PT{0}H{1}M", (int)Duration.TotalHours, Duration.Minutes);
			}

			set
			{
				Duration = string.IsNullOrEmpty(value) ? TimeSpan.Zero : XmlConvert.ToTimeSpan(value);
			}
		}

		/// <summary>
		/// Формат видео в хранилище.
		/// </summary>
		[XmlElement]
		public VideoFormat VideoFormat
		{
			get;
			set;
		}

		/// <summary>
		/// Устанавливает состояние хранилища в соответствии со значениями, загруженными из XML.
		/// </summary>
		/// <param name="mediaStorage">Хранилище.</param>
		public void LoadIntoMediaStorage(MediaStorage mediaStorage)
		{
			mediaStorage.Content = Content;
			mediaStorage.Duration = Duration;
			mediaStorage.VideoFormat = VideoFormat;
		}

		/// <summary>
		/// Устанавливает состояние объекта сериализации в соответствии с параметрами хранилища.
		/// </summary>
		/// <param name="mediaStorage">Хранилище.</param>
		public void LoadFromMediaStorage(MediaStorage mediaStorage)
		{
			Content = mediaStorage.Content;
			Duration = mediaStorage.Duration;
			VideoFormat = mediaStorage.VideoFormat;
		}
	}
}
