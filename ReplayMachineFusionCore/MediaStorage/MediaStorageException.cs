using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace ReplayMachineFusion.Core.MediaStorage
{
	[Serializable]
	public class MediaStorageException : ApplicationException
	{
		public MediaStorageException()
		{
		}

		public MediaStorageException(string message)
			: base(message)
		{
		}

		public MediaStorageException(string message, Exception inner)
			: base(message, inner)
		{
		}

		protected MediaStorageException(SerializationInfo info, StreamingContext context)
			: base(info, context)
		{
		}
	}
}
