﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace ReplayMachineFusion.Core.MediaStorage
{
	[Serializable]
	public class MediaStorageBusyException : MediaStorageException
	{
		public MediaStorageBusyException()
		{
		}

		public MediaStorageBusyException(string message)
			: base(message)
		{
		}

		public MediaStorageBusyException(string message, Exception inner)
			: base(message, inner)
		{
		}

		protected MediaStorageBusyException(SerializationInfo info, StreamingContext context)
			: base(info, context)
		{
		}
	}
}
