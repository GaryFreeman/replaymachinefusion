﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace ReplayMachineFusion.Core.MediaStorage
{
	[Serializable]
	public class MediaStorageNotFoundException : MediaStorageException
	{
		public MediaStorageNotFoundException()
		{
		}

		public MediaStorageNotFoundException(string message)
			: base(message)
		{
		}

		public MediaStorageNotFoundException(string message, Exception inner)
			: base(message, inner)
		{
		}

		protected MediaStorageNotFoundException(SerializationInfo info, StreamingContext context)
			: base(info, context)
		{
		}
	}
}
