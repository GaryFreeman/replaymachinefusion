﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLog;
using ReplayMachineFusion.Core;
using ReplayMachineFusion.Core.MediaStorage;

namespace ReplayMachineFusion.Core
{
	/// <summary>
	/// Главный класс системы, который управляет общим состоянием машины повторов.
	/// </summary>
	public class ReplayMachine : Singleton<ReplayMachine>, IDisposable
	{
		private static Logger logger = LogManager.GetCurrentClassLogger();

		private bool disposed = false;
		private List<MediaStorage.MediaStorage> mediaStorages = new List<MediaStorage.MediaStorage>();
		private List<Channel.Channel> channels = new List<Channel.Channel>();
		private bool isRecording = false;

		private ReplayMachine()
		{
			logger.Info("Машина повторов создана.");

			LoadMediaStorages();
		}

		/// <summary>
		/// Загруженные медиа-хранилища.
		/// </summary>
		public IReadOnlyList<MediaStorage.MediaStorage> MediaStorages
		{
			get
			{
				return mediaStorages;
			}
		}

		/// <summary>
		/// Каналы машины повторов.
		/// 
		/// Канал - камера, с которой производится запись и которая используется для повтора.
		/// </summary>
		public IList<Channel.Channel> Channels
		{
			get
			{
				return channels;
			}
		}

		/// <summary>
		/// Производится ли запись.
		/// </summary>
		public bool IsRecording
		{
			get
			{
				return isRecording;
			}

			set
			{
				isRecording = value;

				foreach (var channel in Channels) {
					channel.IsRecording = IsRecording;
				}
			}
		}

		/// <summary>
		/// Загружает медиа-хранилища.
		/// </summary>
		private void LoadMediaStorages()
		{
			// TODO: загрузка медиа хранилищ.

			logger.Info("Медиа-хранилища загружены.");
		}

		/// <summary>
		/// Проверяет, что объект еще не освобожден ("Disposed").
		/// </summary>
		/// <exception cref="ObjectDisposedException">Ранее был вызван метод Dispose.</exception>
		private void CheckDisposed()
		{
			if (disposed) {
				throw new ObjectDisposedException(GetType().FullName);
			}
		}

		public void Dispose()
		{
			disposed = true;

			logger.Info("Машина повторов удалена.");
		}
	}
}
