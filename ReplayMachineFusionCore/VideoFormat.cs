﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ReplayMachineFusion.Core
{
	/// <summary>
	/// Параметры видео.
	/// </summary>
	[XmlRoot]
	public class VideoFormat
	{
		/// <summary>
		/// Разрешение.
		/// </summary>
		public struct ResolutionType
		{
			/// <summary>
			/// Ширина.
			/// </summary>
			[XmlAttribute]
			public int Width
			{
				get;
				set;
			}

			/// <summary>
			/// Высота.
			/// </summary>
			[XmlAttribute]
			public int Height
			{
				get;
				set;
			}

			public override bool Equals(object obj)
			{
				if (obj == null || obj is ResolutionType == false) {
					return false;
				}

				var right = (ResolutionType)obj;

				return Width == right.Width && Height == right.Height;
			}

			public override int GetHashCode()
			{
				unchecked {
					int hash = 17;

					hash = hash * 23 + Width.GetHashCode();
					hash = hash * 23 + Height.GetHashCode();

					return hash;
				}
			}

			public override string ToString()
			{
				return Width + "x" + Height;
			}

			public static bool operator ==(ResolutionType left, ResolutionType right)
			{
				return left != null && left.Equals(right);
			}

			public static bool operator !=(ResolutionType left, ResolutionType right)
			{
				return !(left == right);
			}
		}

		/// <summary>
		/// ТВ-стандарт.
		/// </summary>
		public enum TvStandartType
		{
			Unknown,
			PAL,
			NTSC			
		}

		/// <summary>
		/// Развертка.
		/// </summary>
		public enum ScanningType
		{
			Interlaced,
			Progressive
		}

		/// <summary>
		/// Виде-кодек.
		/// </summary>
		public enum VideoCodecType
		{
			Unknown,
			DV,
			MPEG2,
			MJPEG
		}

		/// <see cref="TvStandartType"/>
		[XmlElement]
		public TvStandartType TvStandart
		{
			get;
			set;
		}

		/// <see cref="ResolutionType"/>
		[XmlElement]
		public ResolutionType Resolution
		{
			get;
			set;
		}

		/// <summary>
		/// Частота кадров.
		/// </summary>
		[XmlElement]
		public double FrameRate
		{
			get;
			set;
		}

		/// <see cref="ScanningType"/>
		[XmlElement]
		public ScanningType Scanning
		{
			get;
			set;
		}

		/// <see cref="VideoCodecType"/>
		[XmlElement]
		public VideoCodecType VideoCodec
		{
			get;
			set;
		}

		/// <summary>
		/// Битрейт в Мбит/сек.
		/// </summary>
		[XmlElement]
		public int Bitrate
		{
			get;
			set;
		}

		public override bool Equals(object obj)
		{
			if (obj == null || obj is VideoFormat == false) {
				return false;
			}

			var right = obj as VideoFormat;

			return TvStandart == right.TvStandart && Resolution == right.Resolution && FrameRate == right.FrameRate &&
				Scanning == right.Scanning && VideoCodec == right.VideoCodec && Bitrate == right.Bitrate;
		}

		public override int GetHashCode()
		{
			unchecked {
				int hash = 17;

				hash = hash * 23 + TvStandart.GetHashCode();
				hash = hash * 23 + Resolution.GetHashCode();
				hash = hash * 23 + FrameRate.GetHashCode();
				hash = hash * 23 + Scanning.GetHashCode();
				hash = hash * 23 + VideoCodec.GetHashCode();
				hash = hash * 23 + Bitrate.GetHashCode();

				return hash;
			}
		}

		public static bool operator ==(VideoFormat left, VideoFormat right)
		{
			return left != null && left.Equals(right);
		}

		public static bool operator !=(VideoFormat left, VideoFormat right)
		{
			return !(left == right);
		}
	}
}
