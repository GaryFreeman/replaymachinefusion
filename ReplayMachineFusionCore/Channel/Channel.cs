﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLog;

namespace ReplayMachineFusion.Core.Channel
{
	/// <summary>
	/// Канал машины повторов.
	/// 
	/// Представляет из себя одну камеру.
	/// </summary>
	public class Channel
	{
		private Logger logger = LogManager.GetCurrentClassLogger();
		private bool isRecording = false;

		public Channel(string title, CaptureDevice captureDevice)
		{
			Title = title;
			CaptureDevice = captureDevice;

			logger.Info("Канал {0} создан.", Title);
		}

		/// <summary>
		/// Отображаемое имя канала.
		/// </summary>
		public string Title
		{
			get;
			set;
		}

		/// <summary>
		/// Устройство захвата, с которым связан канал.
		/// </summary>
		public CaptureDevice CaptureDevice
		{
			get;
			private set;
		}

		/// <summary>
		/// Производится ли запись канала.
		/// </summary>
		public bool IsRecording
		{
			get
			{
				return isRecording;
			}

			set
			{
				isRecording = value;
			}
		}
	}
}
