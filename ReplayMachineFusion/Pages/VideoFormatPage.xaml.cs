﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ReplayMachineFusion.Core;

namespace ReplayMachineFusion.Pages
{
	public partial class VideoFormatPage : Page
	{
		private ObservableCollection<VideoFormat> supportedVideoFormats = new ObservableCollection<VideoFormat>();

		public VideoFormatPage()
		{
			InitializeComponent();

			supportedVideoFormats.CollectionChanged += SupportedVideoFormatsChanged;
		}

		/// <summary>
		/// true, если формат выбран.
		/// </summary>
		public bool IsFormatSelected
		{
			get
			{
				return CheckField(TvStandart) && CheckField(Resolution) && CheckField(FrameRate) &&
					CheckField(VideoCodec) && CheckField(Bitrate);
			}
		}

		public VideoFormat SelectedFormat
		{
			get
			{
				return new VideoFormat {
					TvStandart = GetFieldValue<VideoFormat.TvStandartType>(TvStandart),
					Resolution = GetFieldValue<VideoFormat.ResolutionType>(Resolution),
					FrameRate = GetFieldValue<double>(FrameRate),
					VideoCodec = GetFieldValue<VideoFormat.VideoCodecType>(VideoCodec),
					Bitrate = GetFieldValue<int>(Bitrate)
				};
			}
		}

		/// <summary>
		/// Список поддерживаемых форматов видео.
		/// </summary>
		public IList<VideoFormat> SupportedVideoFormats
		{
			get
			{
				return supportedVideoFormats;
			}
		}

		private bool CheckField(ComboBox field)
		{
			return field.Visibility != Visibility.Visible || field.SelectedIndex != -1;
		}

		private void ClearField(ComboBox field)
		{
			for (; field.Items.Count > 0;) {
				field.Items.RemoveAt(0);
			}
		}

		private void ClearFields()
		{
			ClearField(TvStandart);
			ClearField(Resolution);
			ClearField(FrameRate);
			ClearField(VideoCodec);
			ClearField(Bitrate);
		}

		private void FillField<T>(ComboBox field, IEnumerable<T> values)
		{
			var valuesSet = new HashSet<T>(values);

			// Прячем ComboBox.
			field.Visibility = (valuesSet.Count < 2) ? Visibility.Collapsed : Visibility.Visible;

			// Прячем надпись.
			int row = Grid.GetRow(field);
			Grid.RowDefinitions[row].Height = (valuesSet.Count < 2) ? new GridLength(0) : GridLength.Auto;

			foreach (var val in valuesSet) {
				var item = new ComboBoxItem();
				item.Content = val.ToString();
				item.Tag = val;

				field.Items.Add(item);
			}
		}

		private T GetFieldValue<T>(ComboBox field)
		{
			if (field.Visibility == Visibility.Visible) {
				var item = field.SelectedItem as ComboBoxItem;
				return (T)item.Tag;
			} else {
				return (T)typeof(VideoFormat).GetProperty(field.Name).GetValue(SupportedVideoFormats[0]);
			}
		}

		private IEnumerable<T> GetPropertyValues<T>(IEnumerable<VideoFormat> videoFormats, string propertyName)
		{
			foreach (var videoFormat in videoFormats) {
				var properties = videoFormat.GetType().GetProperties();
				foreach (var property in properties) {
					if (property.Name == propertyName) {
						yield return (T)property.GetValue(videoFormat);
					}
				}
			}
		}

		private void SupportedVideoFormatsChanged(object sender, NotifyCollectionChangedEventArgs e)
		{
			ClearFields();

			FillField(TvStandart, GetPropertyValues<VideoFormat.TvStandartType>(supportedVideoFormats, "TvStandart"));
			FillField(Resolution, GetPropertyValues<VideoFormat.ResolutionType>(supportedVideoFormats, "Resolution"));
			FillField(FrameRate, GetPropertyValues<double>(supportedVideoFormats, "FrameRate"));
			FillField(VideoCodec, GetPropertyValues<VideoFormat.VideoCodecType>(supportedVideoFormats, "VideoCodec"));
			FillField(Bitrate, GetPropertyValues<int>(supportedVideoFormats, "Bitrate"));
		}
	}
}
