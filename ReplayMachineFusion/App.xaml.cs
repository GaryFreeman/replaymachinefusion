﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using NLog;
using ReplayMachineFusion.Core;

namespace ReplayMachineFusion
{
	public partial class App : Application
	{
		private static Logger logger = LogManager.GetCurrentClassLogger();

		public App()
		{
			AppDomain.CurrentDomain.UnhandledException += (s, e) => {
				if (e.IsTerminating) {
					logger.Fatal(e.ExceptionObject as Exception);
				} else {
					logger.Error(e.ExceptionObject as Exception);
				}
			};

			Startup += (s, e) => logger.Info("Приложение запущено.");

			Exit += (s, e) => {
				ReplayMachine.Instance.Dispose();

				logger.Info("Приложение остановлено.");
			};
		}
	}
}
