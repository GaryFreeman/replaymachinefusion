﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ReplayMachineFusion.Windows.Settings
{
	/// <summary>
	/// Interaction logic for SettingsWindow.xaml
	/// </summary>
	public partial class SettingsWindow : Window
	{
		[DllImport("user32.dll")]
		private static extern int GetWindowLong(IntPtr hWnd, int nIndex);
		[DllImport("user32.dll")]
		private static extern int SetWindowLong(IntPtr hWnd, int nIndex, int dwNewLong);

		private const int GWL_STYLE = -16;
		private const int WS_MAXIMIZEBOX = 0x10000;
		private const int WS_MINIMIZEBOX = 0x20000;

		public SettingsWindow()
		{
			InitializeComponent();

			SectionSelector.SelectedItemChanged += NewSectionSelected;
			SourceInitialized += (s, e) => {
				// Скрываем кнопки "Свернуть" и "Развернуть".

				IntPtr handle = new WindowInteropHelper(this).Handle;

				SetWindowLong(handle, GWL_STYLE, GetWindowLong(handle, GWL_STYLE) & ~WS_MAXIMIZEBOX &
					~WS_MINIMIZEBOX);
			};
		}

		private void NewSectionSelected(object sender, RoutedPropertyChangedEventArgs<object> e)
		{
			var tag = ((sender as TreeView).SelectedItem as TreeViewItem).Tag as string;
			var newPage = "Pages/" + tag + ".xaml";
			ContentArea.Source = new Uri(newPage, UriKind.Relative);
		}
	}
}
