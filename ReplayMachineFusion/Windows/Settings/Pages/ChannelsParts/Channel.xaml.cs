﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Ookii.Dialogs.Wpf;

namespace ReplayMachineFusion.Windows.Settings.Pages.ChannelsParts
{
	public partial class Channel : Page
	{
		public Channel()
		{
			InitializeComponent();
			NewChannel = true;

			SelectFolder.Click += (s, e) => {
				VistaFolderBrowserDialog dialog = new VistaFolderBrowserDialog();
				dialog.Description = "Выберите директорию канала";
				dialog.UseDescriptionForTitle = true;

				if (dialog.ShowDialog(Window.GetWindow(this)).GetValueOrDefault(false)) {
					FolderPath.Text = dialog.SelectedPath;
				}
			};

			ChannelTitle.TextChanged += (s, e) => {
				if (ChannelTitleChanged != null) {
					ChannelTitleChanged(s, e);
				}
			};
		}

		/// <summary>
		/// Возникает, когда название канала изменяется.
		/// </summary>
		public event TextChangedEventHandler ChannelTitleChanged;

		/// <summary>
		/// true, если это новый канал, т.е. он был только что создан кнопкой "Создать канал".
		/// </summary>
		public bool NewChannel
		{
			get;
			private set;
		}
	}
}
