﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ReplayMachineFusion.Windows.Settings.Pages
{
	/// <summary>
	/// Interaction logic for Channels.xaml
	/// </summary>
	public partial class Channels : Page
	{
		public Channels()
		{
			InitializeComponent();

			ChannelsList.SelectionChanged += NewChannelSelected;
			AddChannel.Click += (s, e) => {
				var item = new ListBoxItem();
				item.Content = "Новый канал";

				ChannelsList.Items.Add(item);
				ChannelsList.SelectedItem = item;
			};
		}

		private void NewChannelSelected(object sender, SelectionChangedEventArgs e)
		{
			var selectedChannel = ChannelsList.SelectedItem as ListBoxItem;
			if (selectedChannel != null) {
				ChannelPage.Source = new Uri("ChannelsParts/Channel.xaml", UriKind.Relative);
				ChannelPage.LoadCompleted += (s, ev) => {
					var channel = ev.Content as ChannelsParts.Channel;
				};
			} else {
				ChannelPage.Source = null;
			}
		}
	}
}
