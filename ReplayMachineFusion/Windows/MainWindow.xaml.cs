﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ReplayMachineFusion.Core;
using ReplayMachineFusion.Core.Channel;
using ReplayMachineFusion.Core.MediaStorage;

namespace ReplayMachineFusion.Windows
{
	public partial class MainWindow : Window
	{
		public MainWindow()
		{
			InitializeComponent();

			NewEvent.Click += (s, e) => {
				IReadOnlyList<CaptureDevice> devices = CaptureDevice.GetAvailableCaptureDevices();
				foreach (var videoFormat in devices[0].SupportedVideoFormats) {
					Console.WriteLine("{0}{1}x{2} {3}{4} {5}", (videoFormat.TvStandart !=
						VideoFormat.TvStandartType.Unknown) ? videoFormat.TvStandart + " " : "",
						videoFormat.Resolution.Width, videoFormat.Resolution.Height, videoFormat.FrameRate,
						(videoFormat.Scanning == VideoFormat.ScanningType.Progressive) ? "p" : "i",
						videoFormat.VideoCodec);
				}
			};
		}

		private void MenuItemClick(object sender, RoutedEventArgs e)
		{
			string tag = (sender as MenuItem).Tag as string;
			switch (tag) {
				case "Settings":
					var settingsWindow = new Settings.SettingsWindow();
					settingsWindow.Owner = this;
					settingsWindow.ShowDialog();
					break;
			}
		}
	}
}
